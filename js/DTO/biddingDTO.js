export default class biddingDTO{
    constructor(
        _idBidding,
        _offerDni,
        _offerName,
        _initDate,
        _endDate,
        _amount, 
        _primacy,
        _comment
    ){
        this.idBidding = _idBidding;
        this.offerDni = _offerDni;
        this.offerName = _offerName;
        this.initDate = _initDate;
        this.endDate = _endDate;
        this.amount = _amount; 
        this.primacy = _primacy;
        this.comment = _comment;  
    }
}