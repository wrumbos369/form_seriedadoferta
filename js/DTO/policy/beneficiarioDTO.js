export default class beneficiarioDTO{
    constructor(
        _numCorrelativo,
        _cdbeneficiario,
        _persona,
        _tipoDocumento
    ){
        this.numCorrelativo = _numCorrelativo;
        this.cdbeneficiario = _cdbeneficiario; 
        this.persona = _persona;
        this.tipoDocumento = _tipoDocumento; 
    }
}