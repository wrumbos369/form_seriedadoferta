export default class tomadorpersonaDTO{

    constructor(
        _tipoPersona,
        _dni,
        _dsnombres,
        _nmsecDni,
        _primerNombre,
        _segundoNombre,
        _apellidoPaterno,
        _apellidoMaterno, 
        _nmTelefono1, 
        _nmTelefono2, 
        _dsemail,
        _codEstadoCivil,
        _indSepPatrimonial,
        _conyuge,
        _dsdireccion
    ){
        this.tipoPersona = _tipoPersona;
        this.dni = _dni; 
        this.dsnombres = _dsnombres; 
        this.nmsecDni = _nmsecDni;
        this.primerNombre = _primerNombre;
        this.segundoNombre = _segundoNombre;
        this.apellidoPaterno = _apellidoPaterno;
        this.apellidoMaterno = _apellidoMaterno; 
        this.nmTelefono1 = _nmTelefono1; 
        this.nmTelefono2 = _nmTelefono2; 
        this.dsemail = _dsemail;
        this.codEstadoCivil = _codEstadoCivil; 
        this.indSepPatrimonial = _indSepPatrimonial; 
        this.conyuge = _conyuge;
        this.dsdireccion = _dsdireccion; 

    }
}