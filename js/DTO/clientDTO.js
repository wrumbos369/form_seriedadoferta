export class clientDTO{
    constructor(
        _nameClient,
        _dni,
        _typeClient,
        _region,
        _clientEmail,
        _clientAddress,
        _clientComuna,
        _clientPhone
        
    ){
        this.nameClient = _nameClient; 
        this.dni = _dni; 
        this.typeClient = _typeClient;
        this.region = _region;
        this.clientEmail = _clientEmail;
        this.clientAddress = _clientAddress;
        this.clientComuna = _clientComuna;
        this.clientPhone = _clientPhone;  

    }
}