export default class regionDTO{
         constructor(
             _codigo,
             _valor,
             _provincias
         ){
             this.codigo = _codigo; 
             this.valor = _valor;
             this.provincias = _provincias; 
         }
}