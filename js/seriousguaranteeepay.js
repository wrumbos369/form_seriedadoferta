import  searchDTO  from './DTO/searchDTO.js';
import regionDTO from './DTO/regionDTO.js';
import validuserDTO from './DTO/validators/validuserDTO.js';
import validatebiddingDTO from './DTO/validators/validatebiddingDTO.js'
import biddingDTO from './DTO/biddingDTO.js';
import policyDTO from './DTO/policy/policyDTO.js';
import policypayDTO from './DTO/policy/policypayDTO.js';
import payDTO from './DTO/payDTO.js';
import tipoPersonaDTO from './DTO/policy/tipoPersonaDTO.js';
import tomadorpersonaDTO from './DTO/policy/tomadorpersonaDTO.js';
import glosaDTO from './DTO/policy/glosaDTO.js';
import beneficiarioDTO from './DTO/policy/beneficiarioDTO.js';
import comunaDTO from './DTO/comunaDTO.js';

import regioninputDTO from './DTO/policy/regioninputDTO.js'
import comunainputDTO from './DTO/policy/comunainputDTO.js'




//Variables globlales

var regions = {};
var clienttypes = {}; 
var boolpesosincomment = false; 
var textglosa = ""; 
var textglobal = "{Escriba texto adicional en el cuadro superior}";
var text1 ="PARA GARANTIZAR SERIEDAD DE LA OFERTA LICITACIÓN N°";
var text2 = "SE DEJA EXPRESAMENTE ESTABLECIDO QUE EL MONTO ASEGURADO INDICADO EN LA MISMA CORRESPONDE A $";
var listvalidmessage = []; 
var amountbidding = '';
var idbidding; 
var dnimsectomador; 
var spinnvalidate = false
var clienttypeeval = ''; 
var textopesos = ''; 

// variable booleana que 
var calprimacy = false;

var urlenviromentguaranteepersona = ''
var urlenviromentguaranteenegocio=''
var urlenviromentguaranteegarantia = ''; 
var urlenviromentpay = '';
var urlapitools = '';
var enviroment = 'dev'; // Cambiar esto para cambiar de ambiente. 


if ( enviroment == 'prod' ){

    urlenviromentguaranteenegocio = 'https://ivbna8zqa4.execute-api.us-west-2.amazonaws.com/prod_SegIng';
    urlenviromentguaranteepersona = 'https://ivbna8zqa4.execute-api.us-west-2.amazonaws.com/prod_SegIng';
    urlenviromentguaranteegarantia = 'https://ivbna8zqa4.execute-api.us-west-2.amazonaws.com/prod_SegIng';
    urlenviromentpay = 'https://ivbna8zqa4.execute-api.us-west-2.amazonaws.com/prod_pay';
    urlapitools = 'https://ivbna8zqa4.execute-api.us-west-2.amazonaws.com/prod_portalclientes';

}else if( enviroment =='dev'){

    urlenviromentguaranteenegocio = 'https://nhlen4xc2a.execute-api.us-west-2.amazonaws.com/dev_SegIng';
    urlenviromentguaranteepersona = 'https://nhlen4xc2a.execute-api.us-west-2.amazonaws.com/dev_SegIng';
    urlenviromentguaranteegarantia = 'https://nhlen4xc2a.execute-api.us-west-2.amazonaws.com/dev_SegIng'
    urlenviromentpay = 'https://nhlen4xc2a.execute-api.us-west-2.amazonaws.com/dev_pay';
    urlapitools = 'https://nhlen4xc2a.execute-api.us-west-2.amazonaws.com/dev_portalclientes';

}


$( document ).ready(function(){
    //datepicker
    $(function () {
       $('#datetimepicker1').datetimepicker({
            format: 'DD/MM/YYYY',
            locale: 'es'
        });
    });
    $(function () {
        $('#datetimepicker2').datetimepicker({
            format: 'DD/MM/YYYY',
            locale: 'es',
        });
    });

    var token = getUrlParameter('id');
       if( token ){
           getvoucherdetails( token );
           downloadpdfclick( token );
       } else{
           loadforminfo();
       }
    
    setInputFilter(document.getElementById("amountbidding"), function(value) {
        return /^-?\d*[.,]?\d*$/.test(value); });   

    $('#user-details').hide();
    $('#bidding-details_input').hide();
    
    evalrangedate()
    searchclient();
    selectchangeregion();
    calculateamountprimacy();
    clearinputs();
    confirmtostep2();
    continuestep2();
    allowchardni();
    followstep2();
    formatrut();
    changeinitdate();
    changeclienttype();
    validatePay();
    gettextarea();
    calprimacyamountinputs();
    showpesosincomment();
    confirmandvalidate();
    goback();
});

// FUNCIONES CLICK
// Busca los datos de un cliente según su rut.
function searchclient(){
    $( '#searchclient' ).click( function(){
        
        let formatrut = $('#rut').val().replace(/\./g,'');

        let rut = formatrut.replace('-',''); 
        console.log( "rut:"+rut );
        
        let bidding = $('#licitacion').val().toUpperCase(); 
        if( $.validateRut( rut ) && validateBiddingCode(bidding)){
            let search = new searchDTO( rut, bidding );
            showloadicon( true, $( '#loading-search' ) ) 
            $('#rut').attr('disabled', 'disabled');
            $('#licitacion').attr('disabled', 'disabled'); 
            $('#searchclient').attr('disabled', 'disabled');
            $('#searchclient').removeClass('continue');
            $('#searchclient').addClass('search-disabled');
            flowuserdata( search );
        } else {

            if( !$.validateRut( rut )  ){
                $.alert({
                    title: '¡Advertencia!',
                    content: 'El rut no es válido!',
                    type: 'orange',
                    icon: 'fa fa-warning',
                });  
            }

            if( !validateBiddingCode(bidding) ){
                $.alert({
                    title: '¡Advertencia!',
                    content: 'El ID de licitación no es válido!',
                    type: 'orange',
                    icon: 'fa fa-warning',
                });  
            }

            
        }
       
   
      
    });

}

function validateBiddingCode( codeBidding ){

    const KeyBiddingRegex = /^[0-9]{1,}\-[0-9]{1,}\-[A-Z]{2}[0-9]{2}?$/;

    const valid = KeyBiddingRegex.test(codeBidding);

    console.log(valid);

    return valid; 

}

function continuestep2(){
    $('#confirm').click( function (){
        changestep(2);
    })
}

function goback(){
     $('#gobackcliente').click(function(){
         changestep(1);
     });
     $('#gobackbidding').click(function(){
        changestep(2);
    });

}
//Funcion para ocultar o mostrar pesos en glosa.
function showpesosincomment(){
   
    $('#pesocomment').on( 'change',  function(){    
        textopesos = ""; 
        if( $( this ).is(':checked') ){
            boolpesosincomment = true; 
            textopesos = text2 + formatAmount( amountbidding.replace(/\./g,'') );
            textglosa =  text1 + idbidding + '\n'+ textglobal.toUpperCase() + '\n' + textopesos ;   
            $('#comment').text( textglosa );
        }else{
            boolpesosincomment = false; 
            textglosa = ''; 
            textglosa =   text1 + idbidding + '\n'+ textglobal.toUpperCase();
            $('#comment').text( textglosa );
        }    
    });    
  
}


function changeinitdate(){
    $('#datetimepicker1').on( 'dp.change', function( e ){
        if( !( evaluatedate( e.date, 'initdate') ) ){
              $.alert({
                  title: '¡Advertencia!',
                  content: 'La fecha de inicio debe ser a lo mas 15 días antes de la fecha actual',
                  type: 'orange', 
                  icon: 'fa fa-warning'
              });
              $('#initdate').val('');
          } 
    });

    $('#datetimepicker2').on( 'dp.change', function( e ){

        if( !( evaluatedate( e.date, 'enddate') ) ){
            $.alert({
                title: '¡Advertencia!',
                content: 'La fecha final no debe se mayor a 180 días despues de la fecha inicial.',
                type: 'orange',
                icon: 'fa fa-warning'
            });
            $('#enddate').val('');
        }
    } );
}

function formatrut(){
    $('#rut').rut({formatOn: 'blur', validateOn: 'blur'});     

    $('#rut').on('blur', function(){
        $('#rut').val( function(){
            return this.value.toUpperCase();
        });
    })
  
}

function validatePay(){
    $('#validatePay').click( function(){
        $('#payModal').modal('show');   
    } )
}

function confirmtostep2(){
    $('#validateclient').click( function(){
        if( validateclientform() ){ // 
            $('#emailModal').modal('show');
        }else {
           
            let messages = '';
            messages = '<ul>';

            for (let mess of listvalidmessage ){

                messages = messages + '<li>' + mess +'</li>'
            }
            messages = messages + '</ul>';

            $.alert({
                title: '¡Error!',
                content: messages,
                type: 'red',
                icon: 'fa fa-warning',
            });
        }
    }); 
}



function clearinputs(){
    $('input[type=text][name=userinfo]').click( function(){
        $( this ).parent().closest('div').removeClass('has-warning has-feedback');
        $( this ).next().hide();
    } );
    $('select').click( function(){
        $( this ).parent().closest('div').removeClass('has-warning has-feedback');
        $( this ).next().hide();
    } );

}

function evalclienttype( type ){
    
   if( type === 'J'){
       $('#namenature').hide();
       $('#nameenterprise').show()
       $('#validlastname2').hide();
       $('#validsecondname').hide();
       $('#validlastname1').hide();
       clienttypeeval  = 'J'; 
   }else if( type === 'N'){
       $('#namenature').show();
       $('#nameenterprise').hide()
       $('#validlastname2').show();
       $('#validsecondname').show();
       $('#validlastname1').show();
       clienttypeeval  = 'N'; 
   }

}

function followstep2(){
    $('#confirmate').click( function(){
        changestep( 2 );
    });
    
}



function confirmandvalidate(){
     
     $("#senrequest").click( function(){

        if ( validatebiddingform() ){

            validatepolicy(); 
            changestep( 3 );
        } else {
            let messages = '';
            messages = '<ul>';

            for (let mess of listvalidmessage ){

                messages = messages + '<li>' + mess +'</li>'
            }
            messages = messages + '</ul>';
  
            $.alert({
                title: '¡Error!',
                content: messages,
                type: 'red',
                icon: 'fa fa-warning',
            });
        }
     } );
}

function downloadpdfclick( token ){
    $('#downloadpdfpay').click(function(){
            downloadpdf( token );
    });
}

function createpay( payment ){

    $.ajax({
        type: "POST",
        contentType : "application/json",
        url: urlenviromentpay+'/api-pay/createPay',
        data: JSON.stringify( payment ),
        dataType: "JSON",
        success: function ( dataPay ) {
            showloadicon( false, $('#loadvalidateinfo') );
            showloadicon( true, $('#info-validate') );
            $('#token_ws').hide();
            $('#token_ws').val( dataPay.token );
            $('#token_ws').text( dataPay.token );
            $('#panelformwp').attr('action', dataPay['url']);
            showloadicon( 'loadpay', false );
        },
        error: function( data ){
           console.log("Error en la petición del servicio. ");
        }
    }); 
}

/* Funcion para crear pago y otener url de Transbank */

function geturlfrompayment( idPoliza ){
    

    let paydetails = {
        amountGross: parseFloat( $('#primacy').val().replace('.', '').replace(',', '.') ),
        amountGrossPay: parseInt( $('#primacyclp').val().replace(/\./g, '').replace(',', '.') ) ,
        businessId: "G",
        capital: $('#automatecal').val().replace(/\./g, '').replace(',', '.') ,
        companyId: "ACHI",
        currencyPay: "CLP",
        currencyProduct: "CLF",
        dni: $('#dniclient').val(),
        endValidation: $('#enddate').val(),
        iva: 0.01,
        ivaPay: 0.01,
        primacy: parseFloat( $('#primacy').val().replace(/\./g, '').replace(',', '.') ) ,
        primacyPay:parseFloat( $('#primacy').val().replace(/\./g, '').replace(',', '.') ),
        productBeneficiary: $('#nameoffer').val(),
        productConcept: "SERIEDAD OFERTA",
        productCorrelative: "0",
        productClient:$('#idbidding').val(),
        productNumber: "",
        productProposal: idPoliza,
        registry: moment(new Date()).format('DD/MM/YYYY'),
        startValidation: $('#initdate').val()
    }
    
    //para api-pay dev: 3, Prod: 4
    let paymentinfo = new payDTO(
        '1',
        '1',
        'CLP',
        '2018-09-26T19:32:14.181Z',
        $('#dniclient').val(),
        $('#clientemail').val(),
        $('#firstname').val()+' '+$('#lastname1').val(),
        '1',
        'Inicio',
        '3',
        parseInt( $('#primacyclp').val().replace(/\./g, '').replace(',', '.') )
    );

    let payment = {
        pay : paymentinfo, 
        payDetails : [paydetails]
    }

     createpay( payment ); 


} 



//FIN FUNCIONES CLICK

/* SERVICIOS REST */

// busca un rut en SII

/*
function searchclientbyrut( rut ){
   showloadicon( true, $( '#loading-search' ) ) 
   $.ajax({
       type: "GET",
       contentType: "application/json; charset=utf-8",
       dataType: "json",
       url: "https://gargantua.avla.com/clientes/find_by_rut/?rut="+rut+"&format=json",
       data: "data",
       success: function ( dataresponse ) {
                     
            var client = {
                  nameClient: dataresponse.nombre,
                  clientDni: dataresponse.rut
                  // Mas campos acá cuando Ysrael termine de hacer los servicios. 
            }
            $( '#seriousstep' ).fadeIn( "slow" );
            settextinputclient( client ); 
       }
   });   
    
}

*/

//busca un rut en sistema AMARU
function searchclientbyrutamaru( rut ){
   
    $.ajax({
        type:"GET",
        contentType: "application/json; charset=utf8",
        dataType: "json",
        url: urlenviromentguaranteepersona+"/amaru-api-cliente/tomador/consulta/dni/"+rut+"?codEmpresa=ACHI&codModulo=G",
        success: function ( dataresponse ){
            settextinputclient( dataresponse, 'amaru' ); 

        },
        error: function(){
            showloadicon( false, $( '#loading-search' ) ) 
            $('#errortext').text('Error en la busqueda del cliente')
            $('#errorModal').modal('show');
            $('#rut').removeAttr('disabled');
            $('#licitacion').removeAttr('disabled'); 
            $('#searchclient').removeAttr('disabled');
            $('#searchclient').addClass('continue');
            $('#searchclient').removeClass('search-disabled');
        }
    })

}
// obtiene json con las regiones y sus respectivas comunas
function getallregions(){
   
    $.ajax({
        type: "GET",
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        url: urlenviromentguaranteepersona+"/amaru-api-cliente/ubicacion/distritos?codModulo=G&codEmpresa=ACHI&idDepartamento=&idProvincia=&distrito=&maxrow=false",
        data: "data",
        success: function ( dataresponse ){
            setregionselect( dataresponse );    
            gettypeclient();
        }
    });
}

//obtiene datos de la licitacion desde mercado público 
function getbiddingbycode( codebidding ){

        $.ajax({
            type:"GET",
            contentType:"application/json;",
            dataType: "json",
            url:  urlapitools+"/api-tools/tools/getLicitacionesByCodigo?codigo="+codebidding+"&ticket=3DC898B8-9F2F-4F8C-9651-732DCD551E91",
            success: function( dataresponse ){
                
                if( !dataresponse.errorCode ){
                    let bidding = new biddingDTO(
                        dataresponse.Listado[0].CodigoExterno,
                        dataresponse.Listado[0].Comprador.RutUnidad,
                        dataresponse.Listado[0].Comprador.NombreOrganismo,
                        dataresponse.Listado[0].Fechas.FechaInicio,
                        dataresponse.Listado[0].Fechas.FechaFinal
                    );
                    settextinputbidding( bidding );  
                    showloadicon( false, $( '#loading-search' ) ) 
          
                    $('#searchclient').hide();
                    $('#newsearch').show();
                    $( '#seriousstep' ).fadeIn( "slow" ); 
                } else{
                    showloadicon( false, $( '#loading-search' ) );
                    showloadicon( false, $( '#searchclient' ) );
                    showloadicon( true, $( '#newsearch' ) )  
                    console.log('error-bidding')
                    showloadicon( false, $( '#seriousform' ) )
                    $('#errortext').text('No se ha encontrado la licitación ')
                    $('#errorModal').modal('show');
                 
                    
                }
                
            }
        });
}

// obtiene una lista de tipos de cliente
function gettypeclient(){
    $.ajax({
        type: "GET",
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        url: urlenviromentguaranteepersona+"/amaru-api-cliente/catalogo/listado/tipoPersona?codModulo=G&codEmpresa=ACHI",
        success: function ( dataresponse ){
            settypeclientselect( dataresponse );    
            gettypebidding()
        }
    });

}

//obtiene una lista de tipos de póliza 
function gettypebidding(){
    $.ajax({
        type: "GET",
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        url: urlenviromentguaranteegarantia+"/amaru-api-caucion/autoatencion/tipo-poliza/SEROF?codEmpresa=ACHI",
        success: function ( dataresponse ){
            settypebiddingselect( dataresponse ); 
            getvaluetypeexchange();
        }
    });

}

//formatea una fecha en especifico
function formatedate( date ){
    let dateform = moment( date, 'DD/MM/YYYY' );
    let day = dateform.format( 'DD' );
    let month = dateform.format( 'MM' );
    let year = dateform.format( 'YYYY' );
    let fulldate = year + month + day; 
    return fulldate; 
}

function primacycalculate( initDate, endDate, amountAsegurance, rutTaker, codPolicytype ){
    showloadicon( true, $('#loadprimacy') );
    let indate = formatedate( initDate );
    let endate = formatedate( endDate );
    evaluatedate( indate, endate ); 
    $.ajax({
        type:"GET",
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        url: urlenviromentguaranteegarantia+"/amaru-api-caucion/autoatencion/calcularPrima/SEROF?fechaInicio="+indate+"&fechaFin="+endate+"&codTipoMoneda=108&montoAsegurado="+amountAsegurance+"&rutTomador="+rutTaker+"&codTipoPoliza="+codPolicytype+"&codEmpresa=ACHI",
        success:function( dataResponse ){
            showloadicon( false, $('#loadprimacy'));
            let primacy = dataResponse.valPrimaNeta + dataResponse.valPrimaNeta*0.19;
            $('#primacy').val( formatAmount( primacy ) );
            gettypeexchange(dataResponse.valPrimaTotal, 108, 214, 3 )
        }
    });

}

//servicio para obtener el tipo de  cambio
function gettypeexchange ( amount , amountOri, amountDest, option ){
    
    showloadicon( true, $('#loadcalculate'));
    showloadicon( true, $('#loadcalculateclp'));
    console.log( amount );

        $.ajax({
            type:"GET",
            contentType:"application/json; charset=utf-8",
            dataType: "json",
            url:urlenviromentguaranteenegocio+"/amaru-api-global/general/tipocambio/cambio?valMonto="+amount+"&monOrigen="+amountOri+"&monDestino="+amountDest+"&codEmpresa=ACHI",
            success: function( dataresponse ){
                console.log('data response: '+ dataresponse.value)
                showloadicon( false, $('#loadcalculate'));
                showloadicon( false, $('#loadcalculateclp'));
                switch( option ){
                     case 1:
                         $('#typeexchange').val( formatAmount( dataresponse.value ) );
                         showloadicon(  false, $('#loadingform')  );
                         showloadicon(  true, $('#serioustitle') );
                         showloadicon(  true, $('#seriouscontainer') );
                         break; 
                     case 2: 
                         // datos calculo de monto a uf
                         let dateinit = $('#initdate').val();
                         let dateend = $('#enddate').val();
                         let typepol = $('#policytype').val();
                         let dnitaker = $('#dniclient').val();
                         let valueformat = parseFloat(dataresponse.value) + parseFloat( 0.01 )
                         console.log( valueformat );
                         $('#automatecal').val( formatAmount( valueformat )); 
                         primacycalculate( dateinit, dateend, dataresponse.value, dnitaker, typepol );
                         break; 
                     case 3:
                         $('#primacyclp').val( formatAmount( dataresponse.value ) ); 
                         break;
                }
                
            },          
            error: function (xhr, ajaxOptions, thrownError) {
                alert('Error en la carga de datos.')
            }
        
        })
    


}
// servicio para validar póliza antes de proceder con el pago

function validatepolicy(){
    $('#info-validate').hide();
    var policy = new policyDTO(
        $('#policytype').val(),
        '1',
        '108',
        formatedate( $('#initdate').val() ),
        formatedate( $('#enddate').val() ), 
        parseFloat( $('#primacy').val().replace(',','.') ),
        parseFloat( $('#automatecal').val().replace('.','').replace(',','.') ),
        parseFloat( $('#automatecal').val().replace('.','').replace(',','.') ),
        parseInt( $('#amountbidding').val().replace('.', '') )
        );

    var typeperson = new tipoPersonaDTO(
        $('#clienttype option:selected').val(),
        $('#clienttype option:selected').text(),
        $('#clienttype option:selected').text()
        );

    var taker = new tomadorpersonaDTO(
        $('#clienttype').val(),
        $('#dniclient').val().replace(/\./g,'').replace('-',''),
        $('#firstname').val() +' '+ $('#lastname1').val(),
        dnimsectomador,
        $('#firstname').val(),
        $('#secondname').val(),
        $('#lastname1').val(),
        $('#lastname2').val(),
        $('#clientPhone').val(),
        $('#clientPhone').val(),
        $('#clientemail').val(),
        null,
        '',
        null,
        $('#clientAddress').val()
        );
        
    var idbidding = $('#idbidding').val(); 
        
    var glosa = new glosaDTO(
        {
        "montoPesosFormato": '1,166,495,108',
        "texto" : "LOREM IPSUM",
        "mostrarMontoPesos": true,
        "idLicitacion": idbidding
        },
        $('#comment').text(),
        );
        
    var personbeneficiary = new tomadorpersonaDTO(
        '',
        $('#dnioffer').val().replace('.','').replace('-',''),
        $('#nameoffer').val(),
        );



    var comuna = new comunainputDTO(
        $('#clientComuna option:selected').val(),
        $('#clientComuna option:selected').text()
    );
        
    
    let jsondata = bodyrequest( policy, typeperson, taker, dnimsectomador, glosa, personbeneficiary, idbidding, comuna );
    showloadicon( true, $('#loadvalidateinfo') );
    $.ajax({
        type:"POST",
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        url: urlenviromentguaranteegarantia+"/amaru-api-caucion/autoatencion/express/validate/SEROF?codEmpresa=ACHI",
        data: jsondata,
        success: function ( dataresponse ){
           
            createprepolicy( jsondata );
            settextvalidatebidding(); 
            
            
        },
        error: function( xhr, status, error ){
            showloadicon( false, $('#loadvalidateinfo') );
            showloadicon( true, $('#error-validate') );
            let jsonerror = JSON.parse(xhr.responseText)
            $('#errorvalidatetext').text( jsonerror.message );
            showloadicon( false, $('#info-validate') );
        }
    });

}

// Funcion para crear pre-poliza y obtener id de la poliza

function createprepolicy( jsondata ){

    $.ajax({
        type:"POST",
        contentType:"application/json; charset=utf-8",
        dataType: "json", 
        url:  urlenviromentguaranteegarantia+"/amaru-api-caucion/autoatencion/express/preregistro/SEROF?codEmpresa=ACHI&codModulo=",
        data: jsondata,
        success: function ( dataresponse ){
            if( dataresponse.id ){
                geturlfrompayment( dataresponse.id );
            }else{
                showloadicon( false, $('#loadvalidateinfo') );
                showloadicon( true, $('#error-validate') );
                if (dataresponse.message){
                    $('#errorvalidatetext').text( dataresponse.message );
                }
               
                showloadicon( false, $('#info-validate') );
            }
            
        },
        error: function( xhr, data, error ){
             
            let jsonerror = JSON.parse( xhr.responseText );
            showloadicon( false, $('#loadvalidateinfo') );
            showloadicon( true, $('#error-validate') );
            $('#errorvalidatetext').text( jsonerror.message );
            showloadicon( false, $('#info-validate') );
        }
    });
}


//servicio para obtener datos del comprobante de pago
function getvoucherdetails( token ){
    showloadicon( true, $('#loadingform'));
    showloadicon( false, $('#seriouscontainer'));
    showloadicon( false, $('#serioustitle'));
    showloadicon( false, $('#serioussearch'));

    $.ajax({
        type: 'GET',
        url: urlenviromentpay+'/api-pay/getPayOrderDetailsById?id='+token,
        dataType: 'JSON',
        success: function( result ){
            if( result.pay.status === 'APROBADO'){
                sendpolicy( result );
            }
            else{
                showloadicon( false, $('#loadingform'));
                showloadicon( true, $('#seriouscontainer'));
                showloadicon( true, $('#serioustitle'));
                showloadicon( true, $('#seriousstep'));
                changestep( 4 );
                $('#pago-webpay').show();
                $('#formsearch').hide();  
                $('#voucherdetails').hide();
                $('#errorpayvoucher').show();
            }
            
        },
        error: function( data ){
            $('#errorModal').modal('show');
        }
        
    });

}

function downloadpdf( token ){
    showloadicon( true, $('#loadpdf') );
    $('#downloadpdfpay').attr('disabled', 'disabled')
  //  showloadicon('downloadpdficon', true);
    $.ajax({
        

        type: "GET",
        url:  urlenviromentpay+"/api-pay/voucherCompanyPdf?id="+token,
        data: "data",
        dataType: "text",
        success: function ( resultdata ) {
            showloadicon( false, $('#loadpdf') );
            $('#downloadpdfpay').removeAttr('disabled');
            convertbase64toPDF( resultdata )
        },
        
        error: function( data ){
                   $('#errorModal').modal('show');
        }
    });  

}

function sendpolicy( datapay ){
$.ajax({
    type: "PUT",
    url: urlenviromentguaranteegarantia+"/amaru-api-caucion/autoatencion/express/emision/"+datapay.payDetails[0].productProposal+"?codEmpresa=ACHI",
    dataType: "text",
    success: function ( resultdata ){

        showloadicon( false, $('#loadingform'));
        showloadicon( true, $('#seriousstep'));
        showloadicon( true, $('#seriouscontainer'));
        showloadicon( true, $('#serioustitle'));
        changestep( 4 );
        
        $('#formsearch').hide();
        $('#voucherdetails').show();
        $('#pago-webpay').show();
        $('#emailvoucher').text( datapay.pay.mail);
        $('#rutvoucher').text( $.formatRut( datapay.pay.dni ) );
        $('#namevoucher').text( datapay.pay.nameClient );
        $('#paydatevoucher').text( datapay.pay.datePayment );
        $('#mountpayvoucher').text( '$'+ formatAmount( datapay.pay.total, 0 ) );
        $('#detailsvoucher').text('Se ha registrado su pago por $'+ formatAmount( datapay.pay.total )+' el día '+ datapay.pay.datePayment +'.');  
    },
    error: function( xhr, data, error ){
        let errorjson = JSON.parse( xhr.responseText )
        showloadicon( false, $('#loadingform'));
        showloadicon( true, $('#seriouscontainer'));
        showloadicon( true, $('#serioustitle'));
        showloadicon( true, $('#seriousstep'));
        changestep( 4 );
        $('#pago-webpay').show();
        $('#formsearch').hide();  
        $('#voucherdetails').hide();
        $('#erroremmitpolicy').show();
        $('#texterroremmitpolicy').text( errorjson.message );
      
    }
});

}


/*FIN SERVICIOS REST*/



/* FUNCIONES SET INPUT TEXT */





//Ingresa los datos del usuario en los inputs
function settextinputclient( client, option ){

    if( option === 'amaru' && client.objeto ){
        if( client.objeto.persona.tipoPersona.codigo === 'J' ){
            $( '#firstname' ).val( client.objeto.persona.dsnombres );
        }else if ( client.objeto.persona.tipoPersona.codigo ==='N'){
            $( '#firstname' ).val( client.objeto.persona.primerNombre );
        }
        $( '#secondname' ).val( client.objeto.persona.segundoNombre );
        $( '#lastname1' ).val( client.objeto.persona.apellidoPaterno );
        $( '#lastname2' ).val( client.objeto.persona.apellidoMaterno );
        $( '#dniclient' ).val( $.formatRut( client.objeto.persona.dni ) );
        $( '#clienttype' ).val( client.objeto.persona.tipoPersona.codigo );
        evalclienttype( client.objeto.persona.tipoPersona.codigo );
        $( '#clientemail' ).val( client.objeto.persona.dsemail );
        $( '#clientPhone' ).val( client.objeto.persona.nmtelefono1 );
        $( '#clientAddress' ).val( client.objeto.persona.dsdireccion );
        $( '#region' ).val( client.objeto.persona.departamento.codigo );
        changecomuna( client.objeto.persona.departamento );
        $( '#clientComuna' ).val( client.objeto.persona.distrito.codigo ); 
        
        dnimsectomador = client.objeto.persona.nmsecDni; 
    }// opción para poder buscar client en SII
    else if( !client.objeto ){
        $( '#clientname' ).val( '' );
        $( '#dniclient' ).val( '' );
        $( '#clientype' ).val( '' );
        $( '#region' ).val( '' );
        $( '#clientemail' ).val( '' );
        $( '#clientPhone' ).val( '' );
        $( '#clientAddress' ).val( '' );
        $( '#clientComuna' ).val( '' ); 
        $( '#clientPhone').val( '' );
        $('#dniclient').val( $('#rut').val());
        $('#dniclient').attr('disabled','disabled')
    }
    
}

//Ingresa las regiones en el campo listbox de regiones
function setregionselect( listregion ){
    regions = listregion; 
    $('#region').append('<option disabled selected value="0">Seleccione..</option>');
    $.each( listregion, function( key, item ){;
         $('#region').append( '<option value="'+item.codigo+'">'+item.valor+'</option>');        
    });
}

//Ingresa los tipos de cliente
function settypeclientselect( listtype ){
    clienttypes = listtype; 
    $('#clienttype').append('<option disabled selected value="0">Seleccione..</option>');
    $.each( listtype, function( key, item ){
         $('#clienttype').append( '<option value="'+item.codigo+'">'+item.valor+'</option>');        
    });
}

function settypebiddingselect( listtype ){
    clienttypes = listtype; 
    $('#policytype').append('<option disabled selected value="0">Seleccione..</option>');
    $.each( listtype, function( key, item ){
         $('#policytype').append( '<option value="'+item.codigo+'">'+item.valor+'</option>');        
    });
}

//Ingresa los datos de la licitación en los inputs text
function settextinputbidding( bidding ){

    idbidding = bidding.idBidding; 
    $('#idbidding').val( bidding.idBidding );
    $('#dnioffer').val( bidding.offerDni );
    $('#amountbidding').val( bidding.amount );
    $('#primacy').val( bidding.primacy );
    $('#nameoffer').val( bidding.offerName );
    $('#automatecal').val( bidding.automatecal );
    
    setglozatext();
}

//funcion para ingresar texto adicional en glosa
function setglozatext(){
        
        if( $( '#pesocomment' ).is(':checked') ){
            textopesos = text2 + formatAmount( amountbidding.replace(/\./g,'')  ); 
            textglosa = text1 + idbidding + '\n'+ textglobal.toUpperCase()+ '\n' + textopesos;
        }else{
            textglosa = text1 + idbidding + '\n'+ textglobal.toUpperCase();
        }
        $('#comment').text( textglosa ); 
}

// funcion para ingresar los datos validados en los parrafos del tercer paso

function settextvalidatebidding(){
    //tomador
    $('#infonameclient').text( $('#firstname').val() + ' ' + $('#lastname1').val() ); 
    $('#infodniclient').text( $.formatRut( $('#dniclient').val() ) );
    $('#infotypeclient').text( $('#clienttype option:selected').text());
    $('#infoaddress').text( $('#clientAddress').val() );
    $('#inforegion').text( $('#region option:selected').text() );
    $('#infocomuna').text( $('#clientComuna option:selected').text() );
    $('#infoemail').text( $('#clientemail').val());
    $('#infophone').text( $('#clientPhone').val() );

    //licitación 
    $('#infoidbidding').text( $('#idbidding').val() );
    $('#infodnioffer').text( $('#dnioffer').val() );
    $('#infonameoffer').text( $('#nameoffer').val() );
    $('#infoinitdate').text( $('#initdate').val() );
    $('#infoenddate').text( $('#enddate').val() );
    $('#infoamount').text( $('#amountbidding').val() ); 
    $('#infoautomatecal').text( $('#automatecal').val() );
    $('#infoprimacy').text( $('#primacy').val() );
    $('#infocomment').text( $('#comment').text() );

}

/* FIN FUNCIONES SET INPUT TEXT */


//FLUJOS PRINCIPALES PARA CADA FORMULARIO

function flowuserdata( searchDTO ){
   
    //getallregions(), gettypeclient(), searchclientbyrutamaru( searchDTO.dniclient ),
    var functions = [  searchclientbyrutamaru( searchDTO.dniclient ), getbiddingbycode( searchDTO.idBidding ) ];
    executeSynchornously( functions, 500 );    

}

function loadforminfo(){
   
    getallregions()
    

}


//Spin de carga
function showloadicon( option, comp ){
    
    if ( option ){
         comp.show();
    }
    else{
        comp.hide(); 
    }
}

//ejecuta request de forma sincronica 
function executeSynchornously( functions, timeout ){
    for( var i = 0; i < functions.lenght; i++ ){
       functions[i] 
    }
}




function changestep( step ){
    $('#searchclient')
    switch ( step ){
        case 1 :
            $('#tab1').addClass('active');
            $('#tab2').removeClass('active');
            $('#tab3').removeClass('active');
            $('#tab4').removeClass('active');
            $('#step1').removeClass('gris');
            $('#step1').addClass('azul');
            $('#step2').removeClass('azul');
            $('#step2').addClass('gris');
        break;
        
        case 2 : 
            $('#tab2').addClass('active');
            $('#tab1').removeClass('active');
            $('#tab3').removeClass('active');    
            $('#tab4').removeClass('active');
            $('#step1').removeClass('azul');
            $('#step1').addClass('gris');
            $('#step2').removeClass('gris');
            $('#step2').addClass('azul');
        break; 
        
        case 3 :
            $('#tab3').addClass('active');
            $('#tab2').removeClass('active');
            $('#tab1').removeClass('active');
            $('#tab4').removeClass('active');
            $('#step3').removeClass('gris');
            $('#step3').addClass('azul');
            $('#step2').removeClass('azul');
            $('#step2').addClass('gris');
        break; 

        case 4 :
            $('#tab4').addClass('active');
            $('#tab3').removeClass('active');
            $('#tab2').removeClass('active');
            $('#tab1').removeClass('active');
            $('#step4').removeClass('gris');
            $('#step4').addClass('azul');
            $('#step3').removeClass('azul');
            $('#step3').addClass('gris');

    }
}

function selectchangeregion(){
    $('#region').change( function(){
        let regionselect = new regionDTO( $( this ).val(), $('#region option:selected').text() );
        changecomuna( regionselect );          
    });

}

function changeclienttype(){
  $('#clienttype').change(function(){
      if( $('#clienttype option:selected').val() == 'J'){
          $('#validsecondname').hide();
          $('#validlastname1').hide();
          $('#validlastname2').hide();
          $('#namenature').hide();
          $('#nameenterprise').show();
      }
      else{
        $('#validsecondname').show();
        $('#validlastname1').show();
        $('#validlastname2').show();
        $('#namenature').show();
        $('#nameenterprise').hide();

      }
  })

}
function changecomuna( region ){
    var selectcomuna = $('#clientComuna');
    selectcomuna.removeAttr('disabled');
    selectcomuna.find('option').remove();
    for ( let reg in  regions ){
        if ( region.codigo == regions[reg].codigo ){
            selectcomuna.append('<option disabled selected value="0">Seleccione..</option>');
            for( let prov in regions[reg].provincias ){
                if( regions[reg].provincias[prov].codigo == 1301 ){
                    for ( let subprov of regions[reg].provincias[prov].distritos ){
                        selectcomuna.append('<option value="'+subprov.codigo+'">'+subprov.valor+'</option>');
                    }
                }
                if( regions[reg].provincias[prov].codigo != 1301){
                    selectcomuna.append('<option value="'+regions[reg].provincias[prov].codigo+'">'+regions[reg].provincias[prov].valor+'</option>'); 
                }
               
            }
        }
    }
}

function calprimacyamountinputs(){
    $('#policytype').bind('change', function(){
        console.log('policy type change');
        verifitocalculate() 
    });
    $('#datetimepicker1').bind( 'dp.change', function(){
        console.log('date init change');
        verifitocalculate() 
    } );
    $('#datetimepicker2').bind( 'dp.change', function(){
        console.log('date end change');
        verifitocalculate() 
    }); 
}


// Calculo de prima y de monto uf cuando se cambia el valor de fevha inicio, término o tipo de póliza. 
function verifitocalculate(){
    if(  $('#initdate').val() && $('#enddate').val() && $('#amountbidding').val() && $('#policytype').val() ){
        amountbidding = $( '#amountbidding' ).val().replace('.','');
        // datos calculo de prima
        let dateinit = $('#initdate').val();
        let dateend = $('#enddate').val();
        let amountAseg = $('#amountbidding').val().replace(/\./g,'');
        let typepol = $('#policytype').val();
        let dnitaker = $('#dniclient').val();
        // primacycalculate( dateinit, dateend, amountAseg, dnitaker, typepol );
        gettypeexchange( amountAseg, 214, 108, 2 );
        
     
    }
}



//funciones de calculo de prima y monto en UF

function calculateamountprimacy(){
    $('#amountbidding').bind('blur', function( e ){
       
        if( e.type === 'blur' ){
            let amount = $('#amountbidding').val().replace('.','');
            let evalu = evalrangeamount( parseInt( amount ) )
            amountbidding = $( this ).val();
            
            // datos calculo de prima
            let dateinit = $('#initdate').val();
            let dateend = $('#enddate').val();
            let amountAseg = parseInt( amount );
            let typepol = $('#policytype').val();
            let dnitaker = $('#dniclient').val();
            //datos mensajes de alerta
            let message = '<ul>';
            let messagelist = [] ; 
            let valid = true; 
            
            if( amountAseg && evalu ){

                if( !dateinit ){  
                    messagelist.push('Falta fecha inicial para calculo de prima.');
                    valid = false; 
                }
                if( !dateend ){
                    messagelist.push('Falta fecha final para calculo de prima.');
                    valid = false; 
                }
                if( !typepol ){
                    messagelist.push('Falta tipo de póliza para calculo de prima.');
                    valid = false; 
                }
                
                if( !valid ){
                    for ( let mess of messagelist ){
                        message = message + '<li>' + mess + '</li>'
                    }
                    message = message + '</ul>';
                    $.alert({
                        title: '¡Advertencia!',
                        content: 'Se nececitan de los siguientes datos para el cálculo de prima:\n ' + message,
                        type: 'orange',
                        icon: 'fa fa-warning',
                    });
                    
                } else{
                   
                    gettypeexchange( amountAseg, 214, 108, 2 );

                }
              
            }else if( !evalu ){
                $.alert({
                    title: '¡Advertencia!',
                    content: 'El monto solicitado no puede superar los $2.000.000',
                    type: 'orange',
                    icon: 'fa fa-warning',
                });

            }
            $('#amountbidding').val( formatAmount( amount, 0, ',', '.') );
        }
    
        
        setglozatext(); 
    });
}

//obtiene el tipo de cambio actual y lo ingresa al input typeexchange

function getvaluetypeexchange(){
   
    gettypeexchange( 1, 108, 214, 1 );

}


//generador del request body para realizar la validación de poliza

function bodyrequest( policy, typeperson, taker, _dnimsectomador, glosa, personbeneficiary, _idbidding, _comuna ){
    
    var validatebody = {
        "codModulo" : "G",
        "codEmpresa" : "ACHI",
        "codServicio" : "SEROF",
        "poliza" : {
            "idPoliza":"",
            "codTipoPoliza" : policy.codTipoPoliza,
            "tasa" : policy.tasa,
            "moneda" : policy.moneda,
            "fecInicial" : policy.fecInicial,
            "fecFinal" : policy.fecFinal,
            "prima" :policy.prima, 
            "monto" : policy.monto, 
            "montoAsegurado": policy.montoAsegurado,
            "montoAseguradoPesos" : policy.montoAseguradoPesos 
        },
        "tomador":{
            "tipoPersona":{
                "codigo": typeperson.codigo,
                "valor": typeperson.valor,
                "text": typeperson.text
            },
            "dni":taker.dni,
            "dsnombres" : taker.dsnombres,
            "nmsecDni" : taker.nmsecDni,
            "primerNombre": taker.primerNombre,
            "segundoNombre":taker.segundoNombre,
            "apellidoPaterno":taker.apellidoPaterno,
            "apellidoMaterno":taker.apellidoMaterno,
            "nmtelefono1": taker.nmTelefono1,
            "nmtelefono2":taker.nmTelefono2,
            "dsemail": taker.dsemail, 
            "codEstadoCivil": null,
            "indSepPatrimonial": '',
            "conyuge": null, 
            "dsdireccion": taker.dsdireccion,
            "distrito":{
                "codigo": _comuna.comunavalue,
                "valor":  _comuna.comunacodigo
              
            },
        },
        "glosa":{
            "data":glosa.data,
            "descripcion": glosa.descripcion, 
            "parametros": [
                {
                  "id": 6,
                  "tipo": "TEXT",
                  "codigo": "idLicitacion",
                  "label": "Id. Licitacion",
                  "atributos": {
                    "maxlength": "30",
                    "required": "required",
                    "mask": "9999-999-LP99"
                  },
                  "defaultValue": "",
                  "orden": 1
                },
                {
                  "id": 8,
                  "tipo": "TEXTAREA",
                  "codigo": "texto",
                  "label": "Texto Libre",
                  "atributos": {
                    "required": "required"
                  },
                  "defaultValue": "",
                  "orden": 2
                },
                {
                  "id": 7,
                  "tipo": "SWITCH",
                  "codigo": "mostrarMontoPesos",
                  "label": "Mostrar Monto",
                  "atributos": {
                    "isBoolean": "true",
                    "onlyInput": "true"
                  },
                  "defaultValue": "",
                  "orden": 3
                }
              ]

        },
        "beneficiarios":[
            {  
             "numCorrelativo": 1,
             "cdbeneficiario": null,
             "dsdireccion": "",
             "provincia": null,
             "departamento": null,
             "persona":{
                   "tipoPersona":{
                       "codigo":"J",
                       "valor":"Jurídica",
                       "text":"Juridica"
                    },
                    "dni": personbeneficiary.dni.replace('.',''),
                    "dsnombres": personbeneficiary.dsnombres.toUpperCase(),
                    "primerNombre":personbeneficiary.dsnombres.toUpperCase(),
                    "segundoNombre":"",
                    "apellidoPaterno":"",
                    "apellidoMaterno":"",
                    "nmtelefono1":"",
                    "nmtelefono2":"",
                    "dsemail":"",
                    "codEstadoCivil":null,
                    "indSepPatrimonial": "",
                    "conyuge":null,
                    "dsdireccion": "no informada",
                    "distrito":{
                        "codigo": _comuna.comunavalue,
                        "valor":  _comuna.comunacodigo
                    }
                   
            },
            
            "tipoDocumento":{
                    "codigo":"R",
                    "valor": "RUT",
                    "text" : "RUT"
                }
            }
        ],
        "idLicitacion":_idbidding,
        "organizationalUnit":"764666534",
        "groups": [
            "AMARU-VENTA-VERDE",
            "AMARU-SERIEDAD-OFERTA"
        ]
    }

    return JSON.stringify( validatebody ); 
}








/* FLUJOS DE VALIDACIÖN */

//Validación de datos de usuario. 
function validateclientform(){
    listvalidmessage = [];
    let validation = true;
    let validateuser = new validuserDTO(
                       $('#firstname').val(),
                       $('#secondname').val(),
                       $('#lastname1').val(),
                       $('#lastname2').val(),
                       $('#dniclient').val(),
                       $('#clienttype option:selected').val(),
                       $('#clientAddress').val(),
                       $('#region').val(),
                       $('#clientComuna').val(),
                       $('#clientemail').val(),
                       $('#clientPhone').val() );
    

    if( !validateuser.nameeval() ){
        $('#validfirstname').addClass('has-warning has-feedback');
        listvalidmessage.push('Nombre no puede ser vacío.');
        validation = false; 
    }
    if( !validateuser.name2eval() && clienttypeeval === 'N' ){
        $('#validsecondname').addClass('has-warning has-feedback');
        listvalidmessage.push('Primer nombre no puede ser vacío.');
        validation = false; 
    }
    
    if( !validateuser.lastname1eval() && clienttypeeval === 'N' ){
        $('#validlastname1').addClass('has-warning has-feedback');
        listvalidmessage.push('Primer apellido no puede ser vacío.');
        validation = false; 
    }

    if( !validateuser.lastname2eval() && clienttypeeval === 'N' ){
        $('#validlastname2').addClass('has-warning has-feedback');
        listvalidmessage.push('Segundo apellido no puede ser vacío.');
        validation = false; 
    }

    if( !validateuser.ruteval() ){
        $('#validrut').addClass('has-warning has-feedback');  
        $('#dniempty').show();
        listvalidmessage.push('Rut no puede ser vacío o es invalido');
        validation = false;      
    }
    if( !validateuser.typeeval() ){
        $('#validtype').addClass('has-warning has-feedback');
        $('#typeempty').show();
        listvalidmessage.push('Tipo de persona inválido');
        validation = false; 
    } 
    if( !validateuser.emaileval()){
        $('#validemail').addClass('has-warning has-feedback');
        $('#emailempty').show();
        listvalidmessage.push('Correo no puede ser vacío');
        validation = false; 
    } else if ( !validateuser.validatecell_email('email', validateuser.validemail )){
        $('#validemail').addClass('has-warning has-feedback');
        $('#emailwrong').show();
        listvalidmessage.push('Formato de correo inválido');
        validation = false; 
    } else{
        $('#confirmemail').text( validateuser.validemail );
    }                  
    if( !validateuser.phoneeval()){
        $('#validphone').addClass('has-warning has-feedback');
        $('#phoneempty').show();
        listvalidmessage.push('Teléfono celular no puede ser vacío.');
        validation = false; 
    } 
    
    /*else if( !validateuser.validatecell_email('cell', validateuser.validphone ) ){
        $('#validphone').addClass('has-warning has-feedback');
        $('#phonewrong').show();
        listvalidmessage.push('Formato de teléfono celular inválido.');
        validation = false; 
    }*/

    if( !validateuser.regioneval() ){
        $('#validregion').addClass('has-warning has-feedback');
        $('#regionempty').show();
        listvalidmessage.push('Región inválida');
        validation = false; 
    }
    if( !validateuser.comunaeval() ){
        $('#validcomuna').addClass('has-warning has-feedback'); 
        $('#comunaempty').show();
        listvalidmessage.push('Comuna Inválida');
        validation = false; 
    }
    if( !validateuser.addresseval()){
        $('#validaddress').addClass('has-warning has-feedback');
        $('#addressempty').show();
        validation = false; 
        listvalidmessage.push('Dirección no puede ser vacío');
    }

    return validation; 
    

}

function validatebiddingform(){
    listvalidmessage = [];
    let validation = true;
    let validatebidding = new validatebiddingDTO(
                       $('#idbidding').val(),
                       $('#dnioffer').val(),
                       $('#initdate').val(),
                       $('#clienttype option:selected').val(),
                       $('#automatecal').val(),
                       $('#nameoffer').val(),
                       $('#enddate').val(),
                       $('#amountbidding').val(),
                       $('#primacy').val(),
                       $('#addcomment').val() );
    

    if( !validatebidding.idbiddingeval() ){
        $('#valididbidding').addClass('has-warning has-feedback');
        listvalidmessage.push('Id de Licitación no puede ser vacío.');
        validation = false; 
    }
    if( !validatebidding.dnioffereval() ){
        $('#validdnioffer').addClass('has-warning has-feedback');
        listvalidmessage.push('DNI oferente no puede ser vacío.');
        validation = false; 
    }
    
    if( !validatebidding.initdateeval() ){
        $('#validinitdate').addClass('has-warning has-feedback');
        listvalidmessage.push('Fecha inicial no puede ser vacío.');
        validation = false; 
    }

    if( !validatebidding.policytypeeval() ){
        $('#validpolicytype').addClass('has-warning has-feedback');
        listvalidmessage.push('El tipo de póliza es inválido.');
        validation = false; 
    }

    if( !validatebidding.automatecaleval() ){ 
        $('#validautomatecalt').addClass('has-warning has-feedback');
        listvalidmessage.push('El monto en UF no puede ser vacío');
        validation = false;      
    }
    if( !validatebidding.nameoffereval() ){
        $('#validnameoffer').addClass('has-warning has-feedback');
        listvalidmessage.push('Nombre oferente no puede ser vacío.');
        validation = false; 
    } 
    if( !validatebidding.enddateeval() ){
        $('#enddate').addClass('has-warning has-feedback');
        listvalidmessage.push('La fecha de término no puede ser vacía.');
        validation = false; 
    }             
    if( !validatebidding.amountbiddingeval() ){
        $('#validamountbidding').addClass('has-warning has-feedback');
        listvalidmessage.push('El monto en CLP no debe ser vacío.');
        validation = false; 
    }
    if( !validatebidding.primacyeval() ){
        $('#validprimacy').addClass('has-warning has-feedback'); 
        listvalidmessage.push('Prima no puede ser vacío');
        validation = false; 
    }
    if( !validatebidding.commenteval() ){
        console.log( validatebidding.validcomment );
        listvalidmessage.push('Debe ingresar un texto adicional');
        validation = false; 
    }

    return validation; 
    

}



/* FUNCIONES VARIAS */

function gettextarea(){
    setTimeout( $('#addcomment').bind('blur keyup', function(){
       textglobal = $( this ).val();     
       setglozatext(); 
    }), 1000)  
}


function evalrangeamount( amount ){
   if( amount > 2000000 ){
       return false; 
   }else{
       return true;
   }
     
} 




// Funcion para validar solo caracteres numericos
function allowchardni(){
    $('#rut').keypress( function( key ) {
        if( ( key.charCode >= 32 && key.charCode < 47 ) || ( key.charCode >= 58 && key.charCode < 106 ) || ( key.charCode >= 108 && key.charCode < 126 )  ){
            $( this ).val( $( this ).val().replace( key.key, '') );
        }else{
            console.log('perm');
        }
    });
}

function caldaysrange(){
    
    if( moment( $('#initdate').val(), 'DD/MM/YYYY' ) &&  moment( $('#enddate').val(), 'DD/MM/YYYY' )){
        let initdate = moment( $('#initdate').val(), 'DD/MM/YYYY' );
        let enddate =  moment( $('#enddate').val(), 'DD/MM/YYYY' );
        let daysrange = enddate.diff( initdate, 'days' )
        if( daysrange ){
            $('#daysrange').text( daysrange );
        }
        
    }
    
}

function evaluatedate( date, typedate ){
    
    caldaysrange()
    switch( typedate ){
        case 'initdate':    
        //evalua fecha  inicial si es antes de los 15 dias a la fecha actual 
            let actualdate1 = moment(); 
            let difference1 = actualdate1.diff( date, 'days' );
            if ( difference1 > 15 ){
                $('#daysrange').text( '0' )
                $('#datetimepicker1').data("DateTimePicker").clear();
                return false;
            } 
            else{
                return true; 
            }
        case 'enddate':
        //evalua fecha final 
            let initdate = moment( $('#initdate').val(), 'DD/MM/YYYY' );
            let maxenddate2 = initdate.clone().add( 180, 'day');
            let difference2 = maxenddate2.diff( date, 'days' );
            
            if( difference2 < 0 ){
                $('#daysrange').text( '0' )
                $('#datetimepicker2').data("DateTimePicker").clear();
                return false; 
            }
            else{
                return true; 
            }
                
    }
}

function evalrangedate(){

    $('#datetimepicker2').on( 'dp.change', function( e ){

        let initdate =  moment( $('#initdate').val(), 'DD/MM/YYYY' );
        let enddate =   moment( $('#enddate').val(), 'DD/MM/YYYY' );
        let diference = enddate.diff( initdate, 'days');
        if ( diference <= 0 && $('#enddate').val()!= '' ){

            $.alert({
                title: '¡Advertencia!',
                content: 'La fecha de termino no puede ser menor que la fecha inicial',
                type: 'orange',
                icon: 'fa fa-warning',
            });
            $('#enddate').val('');
            
            
        }

    });

    $('#datetimepicker1').on( 'dp.change', function( e ){

        let initdate =  moment( $('#initdate').val(), 'DD/MM/YYYY' );
        let enddate =   moment( $('#enddate').val(), 'DD/MM/YYYY' );
        let diference = enddate.diff( initdate, 'days');
  
        if ( diference <= 0 && initdate ){

            $.alert({
                title: '¡Advertencia!',
                content: 'La fecha de termino no puede ser menor que la fecha inicial',
                type: 'orange',
                icon: 'fa fa-warning',
            });
            $('#initdate').val('');
            
            
        }

    });


} 

    //Funcion para formatear montos
    function formatAmount(amount, decimalCount = 2, decimal = ",", thousands = ".") {
        try {
           decimalCount = Math.abs( decimalCount );
           decimalCount = isNaN( decimalCount ) ? 2 : decimalCount;
       
           const negativeSign = amount < 0 ? "-" : "";
       
           let i = parseInt(amount = Math.abs(Number(amount) || 0).toFixed(decimalCount)).toString();
           let j = (i.length > 3) ? i.length % 3 : 0;
       
           return negativeSign + (j ? i.substr(0, j) + thousands : '') + i.substr(j).replace(/(\d{3})(?=\d)/g, "$1" + thousands) + (decimalCount ? decimal + Math.abs(amount - i).toFixed( decimalCount ).slice(2) : "");
        } catch (e) {
        }
    }

     //Función que retorna el valor del parametro de la url; sParam: el parametro a buscar
     function getUrlParameter( sParam ) {
        var sPageURL = decodeURIComponent(window.location.search.substring(1)),
            sURLVariables = sPageURL.split('&'),
            sParameterName,
            i;
    
        for ( i = 0; i < sURLVariables.length; i++ ) {
            sParameterName = sURLVariables[i].split('=');
    
            if ( sParameterName[0] === sParam ) {
                return sParameterName[1] === undefined ? true : sParameterName[1];
            }
        }
    };

     // Funcion para convertir la data en base64 a pdf y descarga el archivo   
    function convertbase64toPDF( base64 ){
        var a = document.createElement('a');
        var pdf = 'data:application/pdf;base64,'+base64;
        a.setAttribute('type','hidden');
        a.download = 'Comprobante de Pago';
        a.type = 'application/pdf';
        a.href = pdf;
        document.body.appendChild(a);
        a.click();
        a.remove();
    }

    //Funcion para bloquear caracteres en los inputs

    function setInputFilter(textbox, inputFilter) {
        ["input", "keydown", "keyup", "mousedown", "mouseup", "select", "contextmenu", "drop"].forEach(function(event) {
          textbox.addEventListener(event, function() {
            if (inputFilter(this.value)) {
              this.oldValue = this.value;
              this.oldSelectionStart = this.selectionStart;
              this.oldSelectionEnd = this.selectionEnd;
            } else if (this.hasOwnProperty("oldValue")) {
              this.value = this.oldValue;
              this.setSelectionRange(this.oldSelectionStart, this.oldSelectionEnd);
            }
          });
        });
      }



